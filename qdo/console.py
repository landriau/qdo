"""
Console script entry points.
"""

from __future__ import print_function

import sys
import os
from getpass import getuser
from argparse import ArgumentParser
from optparse import OptionParser
import time
import random

from .core import (set_project, print_queues, valid_queue_name, queues,
                   connect, Queue)
from .launch_calc import QDOLaunchException

def qdo(argv=None):
    _qdohelp = """
Implemented Commands:
    qdo list                    : list all available queues
    qdo create   qname          : create qname (optional; ok to just add/load)
    qdo load     qname taskfile [--priority <float>]
                                : load tasks from file into queue
                                  if taskfile=='-' read from stdin instead
    qdo add      qname task [--priority <float>]
                                : add a single task to a queue
    qdo launch   qname nworkers
                             [--batchopts opts] [--script cmd] [--numapack]
                             [--cores_per_worker  <cores-per-worker>]
                             [--mpiproc_per_worker <PEs-per-worker>]
                             [--cores_per_mpiproc <cores-per-PE>]
                             [--jobs <number-of-jobs-to-launch>]
                             [--timeout t] [--walltime HH:MM:SS]
                             [--batchqueue q] [--verbose] [--keep_env]
                             [--env_vars VARNAME1,VARNAME2]
                                : launch jobs to process tasks
    qdo status   qname          : print status of queue and tasks
    qdo tasks    qname [--verbose] : print all tasks in queue
    qdo retry    qname [--priority <float>]
                                : move failed tasks back into pending
    qdo recover  qname [--priority <float>]
                                : move running tasks back into pending
    qdo rerun    qname --force [--exitcode E] [--priority <float>]
                                : move all tasks back into pending, optionally
                                  filtering by exit code
    qdo pause    qname          : temporarily suspend distribution of tasks
    qdo resume   qname          : resume distributing tasks to jobs
    qdo delete   qname --force  : delete queue and db
    qdo do       qname          : run qname if marked active
    
Not yet implemented:
    qdo save   qname file       : checkpoint server queue so it can be stopped
    qdo load   qname file       : reload a previously saved queue
    
All commands except "qdo list" require a queue name to be provided.
"""

    #- Do manual arg parsing because of somewhat non-standard usage.
    #- Consider reorganizing how tasks are specified and parsed.
    #- this is getting a little out of hand.
    parser = OptionParser(usage="qdo command queue_name args [options]")
    parser.add_option("-u", "--user",  type=str,  help="username",
                      default=getuser())
    parser.add_option("-f", "--force", action='store_true',
                      help="override warnings and force command")
    parser.add_option("-a", "--all", action='store_true',
                      help="list queues for all users")
    parser.add_option("-v", "--verbose", action='store_true',
                      help="verbose printing")
    parser.add_option("-t", "--timeout", type=float,
                      help="time to wait for tasks before giving up "
                      "[QDO_TIMEOUT]", default=60)
    parser.add_option("--batchopts", type=str, help="options to pass to qsub",
                      default="")
    parser.add_option("--walltime", dest="walltime", type=str,
                      help="batch job walltime limit")
    parser.add_option("--batchqueue", dest="batchqueue", type=str,
                      help="batch queue name (not qdo queue name)")
    parser.add_option("--state", dest='state', type=str,
                      help="include only tasks in this state")
    parser.add_option("--exitcode", type=int,
                      help="only rerun tasks that exited with this code")
    parser.add_option("--script", dest="script", type=str,
                      help="script to run for each task")
    parser.add_option("--prefix", dest="prefix", type=str,
                      help="command to be appended as a prefix to each task "
                      "(if --script specified, prefix will be appended before "
                      "the script content)")
    parser.add_option("--priority", dest="priority", type=float,
                      help="priority for jobs to be added")
    parser.add_option("--runtime", dest="runtime", type=float,
                      help="quit after running jobs for this many seconds")
    parser.add_option("--jitter", dest="jitter", type=float,
                      help="Startup random jitter time to avoid hitting DB "
                      "too hard")
    parser.add_option("--cores_per_worker", dest="cores_per_worker", type=int, 
                      help="Number of cores to run a worker", default=None)
    parser.add_option("--mpiproc_per_worker", dest="mpiproc_per_worker",
                      type=int, help="Number of PEs required in a worker",
                      default=0)
    parser.add_option("--cores_per_mpiproc", dest="cores_per_mpiproc",
                      type=int, help="Minimum number of cores in each PE "
                      "of a worker. If not set, OMP_NUM_THREADS is set to"
                      " the resulting number of cores per PE.", default=None)
    parser.add_option("--numapack", action="store_true",
                      help="Avoid PEs to be run in more than one NUMA")
    parser.add_option("--jobs", dest="njobs", type=int, 
                      help="Jobs to launch in parallel to divide the workers"
                      " among.", default=1)
    parser.add_option("-V", "--keep_env", action="store_true", 
                      help="launch conserves environment variables at"
                      "execution time.")
    parser.add_option("--env_vars", dest="env_vars", type=str,
                      help="list of env vars to be kept in the job launched. "
                      "Comma separated values of var names", default=None)
    parser.add_option("-p", "--project", dest="project", type=str,
                      help="Sets the project to work on. If not set, it uses "
                      "the user project", default=None)
    parser.add_option("--dead", dest="dead", action="store_true", default=False,
                      help="Recover jobs whose jobid is not currently running")
    parser.add_option("--jobid", help="Recover jobs matching the given 'jobid' exactly")

    if argv is None:         # This is only necessary for the following
        argv = sys.argv[1:]  # manual parsing.

    #- optparse mis-formats the extended help, so intercept help requests
    if (len(set(argv) & {'-h', '-help', '--help'}) > 0 or
            len(argv) == 0 or argv[0] == 'help'):
        parser.print_help()
        print(_qdohelp)
        return 0

    #- Now we can parse the options
    opts, args = parser.parse_args(argv)
    action = args[0]

    if opts.jitter is not None:
        import random
        time.sleep(random.uniform(0.0, opts.jitter))

    if action == "noop":
        return 0

    if opts.project:
        set_project(opts.project)

    if action == 'list':
        if opts.all:
            print_queues()
        else:
            print_queues(opts.user)
        return 0
    elif len(args) == 1:
        sys.stderr.write('You must provide a queue name; qdo -h for details')
        return 1

    name = args[1]
    if not valid_queue_name(name):
        return 2

    if action == 'create':
        if name in queues(user=opts.user).keys():
            print('Queue %s already exists.' % name)
            return 0

    #- Some tasks require an existing queue; others it is optional
    if action in ('create', 'load', 'add'):
        q = connect(name, create_ok=True, user=opts.user)
    else:
        q = connect(name, create_ok=False, user=opts.user)

    if action == 'create':
        print('Queue %s created' % name)
    elif action == 'load':
        task_file = args[2]
        q.loadfile(task_file, priority=opts.priority)
    elif action == 'add':
        task = args[2]
        q.add(task, priority=opts.priority)
    elif action == 'launch':
        nworkers = int(args[2])
        try:
            extra_env_vars=[]
            if opts.env_vars is not None:
                extra_env_vars=opts.env_vars.split(",")
            q.launch(nworkers, njobs=opts.njobs,
                     mpiproc_per_worker=opts.mpiproc_per_worker,
                     cores_per_mpiproc=opts.cores_per_mpiproc,
                     cores_per_worker=opts.cores_per_worker,
                     numa_pack=opts.numapack,
                     script=opts.script, timeout=opts.timeout, 
                     runtime=opts.runtime, batchqueue=opts.batchqueue,
                     walltime=opts.walltime,
                     batch_opts=opts.batchopts, verbose=opts.verbose,
                     export_all_env_vars=opts.keep_env,
                     extra_vars_to_export=extra_env_vars)
        except QDOLaunchException as e:
            sys.stderr.write("Launch "+str(e))
    elif action == 'status':
        q.print_task_state()
    elif action == 'retry':
        n = len(q.retry(exitcode=opts.exitcode, priority=opts.priority))
        if n > 0:
            print("%d tasks reset to pending" % n)
        else:
            print("no failed tasks found to retry")
    elif action == 'recover':
        taskfilter = None

        if opts.dead:
            # Find the list of running jobs, using SLURM command-line tool "sqs"
            import subprocess
            s = subprocess.check_output(['sqs'])
            ''' example:
            JOBID              ST   REASON       USER         NAME         NODES    ...
            4728586_0          R    None         dstn         calibs       4        ...
            4728586_[1-7]      PD   Resources    dstn         calibs       4        ...
            '''
            running_jobs = []
            s = s.split(b'\n')[1:]
            for line in s:
                words = line.split()
                if len(words) < 2:
                    continue
                jobid = words[0]
                jobstate = words[1]
                # print('Job', jobid, 'state', jobstate)
                if jobstate == b'R':
                    running_jobs.append(jobid.decode())
            # print('Running jobs:', ', '.join(running_jobs))

            def jobid_not_running(task):
                jobid = task.jobid
                # Our list of running_jobs only has the JOB_ID and
                # ARRAY_TASK_ID values, not the SLURM_CLUSTER_NAME, so
                # drop the first term off the front of "jobid".
                words = jobid.split('_')
                if len(words) == 0:
                    return False
                jobid = '_'.join(words[1:])
                return not(jobid in running_jobs)

            taskfilter = jobid_not_running

        elif opts.jobid:
            def jobid_matches(task):
                return task.jobid == opts.jobid

            taskfilter = jobid_matches

        n = len(q.recover(priority=opts.priority, taskfilter=taskfilter))
        if n > 0:
            print("%d tasks reset to pending" % n)
        else:
            print("no running tasks found to retry")
    elif action == 'rerun':
        if not opts.force:
            print("ERROR: To prevent accidental resets, you must use "
                  "--force with rerun")
            print("No action taken")
            return 256
        n = len(q.rerun(priority=opts.priority))
        if n > 0:
            print("%d tasks reset to pending" % n)
        else:
            print("no tasks found to rerun")

    elif action == 'tasks':
        q.print_tasks(opts.verbose, state=opts.state,
                      exitcode=opts.exitcode)
    elif action == 'pause':
        q.pause()
        print("Queue %s paused; use 'qdo resume %s' to resume" %
              (name, name))
    elif action == 'resume':
        q.resume()
        print("Queue %s resumed" % name)
    elif action == 'delete':
        if opts.force:
            q.delete()
        else:
            sys.stderr.write("To prevent accidental deletions, you must "
                             "use --force with delete")
            return 256
    elif action == 'do':
        if len(args) > 2:
            print('Warning: arguments ignored:', args[2:])
        if q.state == Queue.ACTIVE:

            # If we're running in a Slurm environment, generate the "jobid"
            # string based on environment variables.
            cluster = os.environ.get('SLURM_CLUSTER_NAME', '')
            jid = os.environ.get('SLURM_JOB_ID', '')
            aid = os.environ.get('SLURM_ARRAY_TASK_ID', '')
            ajid = os.environ.get('SLURM_ARRAY_JOB_ID', '')
            print('SLURM_CLUSTER_NAME', cluster)
            print('SLURM_JOB_ID', jid)
            print('SLURM_ARRAY_TASK_ID', aid)
            print('SLURM_ARRAY_JOB_ID', ajid)

            if len(cluster + jid + aid) == 0:
                jobid = None
            else:
                if len(aid):
                    jobid = '%s_%s_%s' % (cluster, ajid, aid)
                else:
                    jobid = '%s_%s' % (cluster, jid)
            print('Setting jobid', jobid)

            q.do(timeout=opts.timeout, script=opts.script,
                 runtime=opts.runtime, prefix=opts.prefix, jobid=jobid)
        else:
            print("Queue %s is paused; not processing commands" % name)
            print("Use 'qdo resume %s' to resume" % name)
    else:
        print('ERROR: Command "%s" not recognized' % action)
        return 1

    return 0

def qdo_log(argv=None):
    parser = ArgumentParser(description="Grep qdo logs for a particular ID")
    parser.add_argument("id")
    parser.add_argument("logfiles", nargs='+')
    args = parser.parse_args(argv)

    loglines = None
    for logname in args.logfiles:
        with open(logname) as fx:
            for line in fx:
                if line.startswith('## QDO Starting'):
                    thisid = line.split()[3].strip()
                    if thisid == args.id:
                        loglines = list()

                if loglines is not None:
                    loglines.append(line)            
                    # We found the entry we want; break out of parsing
                    # this log file.
                    if (line.startswith('## QDO SUCCESS') or
                        line.startswith('## QDO FAILED')):
                        break

        #- Break out of loop over logfiles
        if loglines is not None:
            break

    if loglines is None:
        print("ERROR: unable to find id", args.id, "in the logfiles")
        return 1
    else:
        print("".join(loglines), end="")
        return 0


def qdo_pgadmin(argv=None):
    _qdo_pgadmin_help = """
Administrate postgresSQL databases used with QDO.

authentication:
    If --user and --pass are used qdo_pgadmin will read user/pass
    information from QDO_DB_USER and QDO_DB_PASS.If these variables
    are not set, qdo_pgadmin will user current uid and no password to
    login into postgress.

database selection:
    If --db is not set, qdo_pgadmin will connect to the db
    QDO_DB_NAME. If this variable is not set it will connect to 'qdo'
    database.

server connection info:
    If --host and/or --port are not set, connection data will be read
    from QDO_DB_HOST and QDO_DB_PORT. If these variables are not set,
    it will connect to localhost:5432.

commands:
    qdo_pgadmin createdb dbname             : create dbname database.
    qdo_pgadmin configdb dbname             : configures existing dbname
                                              database to be used with qdo.
    qdo_pgadmin createuser username passwd  : add user username with passwd
                                              password and creates a personal
                                              project.
    qdo_pgadmin deluser username            : deletes user username (it does not
                                              delete user's project or pending
                                              tasks).
    qdo_pgadmin enable u1 [u2 u3...]        : enables db access to u1, u2, u3...
    qdo_pgadmin disable u1 [u2 u3...]       : disables db access to u1, u2, u3..
                                              User accounts are not deleted.
    qdo_pgadmin listusers                   : list all users, their db access
                                              status and granted projects.
    qdo_pgadmin createprj  p1 [p2 p3...]    : creates new projects p1, p2, p3...
    qdo_pgadmin delprj  p1 [p2 p3...]       : deletes projects p1, p2, p3... 
                                              deleting all queues and pending
                                              tasks. It does not affect running
                                              tasks.
    qdo_pgadmin listprjs                    : lists all the projects and their
                                              granted users.
    qdo_pgadmin grantprj p1 u1 [u2 u3...]   : allows users u1, u2, u3... to
                                              enqueue jobs on queues of project
                                              p1.
    qdo_pgadmin revokeprj p1 u1 [u2 u3...]  : disallows users u1, u2, u3... to
                                              enqueue jobs on queues of project
                                              p1.
    qdo_pgadmin grantuser u1 p1 [p2 p3...]  : allows user u1 to enqueue jobs on
                                              queues of projects p1, p2, p3...  
    qdo_pgadmin revokeuser u1 p1 [p2 p3...] : disallows user u1 to enqueue jobs
                                              on queues of projects p1, p2,
                                              p3... 
"""

    # TODO (gonzalorodrigo): Add a command to list users, projects, users
    # accessing a project and projects accessed by a user
    from .backends import postgres

    # default values
    # TODO(gonzalorodrigo): take this from the postgres back end class.
    host = os.environ.get('QDO_DB_HOST', 'localhost')
    dbname = os.environ.get('QDO_DB_NAME', 'qdo')
    port = os.environ.get('QDO_DB_PORT', '5432')
    user = os.environ.get('QDO_DB_USER', getuser())
    password = os.environ.get('QDO_DB_PASS', None)

    parser = ArgumentParser()

    # Positional arguments
    parser.add_argument("action", action="store",
                        help="action: createdb, adduser, configdb, "
                        "deluser, deluser, enable, disable, listusers, addprj,"
                        " delprj, grantprj, revokeprj, listprjs, grantuser,"
                        " revokeuser")
    parser.add_argument("values", metavar="values", action="store", nargs='*',
                        help = "[u1 u2r u3...] [p1 p2 p3...]")

    #options
    parser.add_argument("--host", action="store", dest="host", default=host,
                        help="Postgres server to connect to.")
    parser.add_argument("--db", action="store", dest="dbname", default=dbname,
                        help="Database name.")
    parser.add_argument("--port", action="store", dest="port", default=port,
                        help="Host connection port.")
    parser.add_argument("--user", action="store", dest="user", default=user,
                        help="User credential to use connection to postgres.")
    parser.add_argument("--pass", action="store", dest="password",
                        default=password, 
                        help="Password credential to use connection to "
                        "postgres.")

    if argv is None:         # This is only necessary for the following
        argv = sys.argv[1:]  # manual parsing.

    #- optparse mis-formats the extended help, so intercept help requests
    if (len(set(argv) & {'-h', '-help', '--help'}) > 0 or
            len(argv) == 0 or argv[0] == 'help'):
        print(_qdo_pgadmin_help)
        return 0

    args = parser.parse_args(argv)

    p_admin = postgres.AdminModule(args.host, args.dbname, args.port,
                                   args.user, args.password)

    # minimum and maximum number of values, function to call on values.
    actioninfo = {
        "createdb": (1, 1, lambda v: p_admin.create_new_db(v[0])),
        "configdb": (1, 1, lambda v: p_admin.configure_db(v[0])),
        "createuser": (2, 2, lambda v: p_admin.create_user(v[0], v[1])),
        "deluser": (1, None, lambda v: p_admin.delete_users(v)),
        "enable": (1, None, lambda v: p_admin.enable_users(v)),
        "disable": (1, None, lambda v: p_admin.disable_users(v)),
        "listusers": (None, None, lambda v: p_admin.list_users()),
        "listprjs": (None, None, lambda v: p_admin.list_projects()),
        "createprj": (1, None, lambda v: p_admin.create_projects(v)),
        "delprj": (1, None, lambda v: p_admin.delete_projects(v)),
        "grantprj": (2, None,
                     lambda v: p_admin.grant_project_users(v[0], v[1:])),
        "revokeprj": (2, None, 
                      lambda v: p_admin.revoke_project_users(v[0], v[1:])),
        "grantuser": (2, None, 
                      lambda v: p_admin.grant_user_projects(v[0], v[1:])),
        "revokeuser": (2, None,
                       lambda v: p_admin.revoke_user_projects(v[0], v[1:]))
    }

    if args.action not in actioninfo:
        print("Command {!r} not defined.".format(args.action))
        return 1
    minvals, maxvals, func = actioninfo[args.action]

    # Explicitly check that length of args.values is appropriate.
    if minvals is not None and len(args.values) < minvals:
        print("Missing arguments, found {:d} expected {:d}"
              .format(len(args.values), minvals))
        return 1
    if maxvals is not None and len(args.values) > maxvals:
        print("Too many arguments, found {:d} expected {:d}"
              .format(len(args.values), maxvals))
        return 1

    func(args.values)
    return 0

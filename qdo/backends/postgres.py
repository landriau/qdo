"""
QDO backend using a postgres database.
Uses psycopg2 for python access.
Implements all features of the backend.

-------------
Configuration
-------------
Enable this backend by setting the environment variable:
export QDO_BACKEND=postgres

And set up the database connection via environment variable:

export QDO_DB_NAME=xxx
export QDO_DB_HOST=xxx
export QDO_DB_PORT=5432
export QDO_DB_USER=xxx
export QDO_DB_PASS=yyy

You can also keep the password in your ~/.pgpass file: eg,

scidb2.nersc.gov:5432:desi:desi_user:PASSWORD

Make sure you "chmod 600 ~/.pgpass" or postgres will ignore it.

(dstn found this didn't work on edison compute nodes, and had to set
QDO_DB_PASS instead.  YMMV.)

Defaults are:
        QDO_DB_NAME -- database name, defaults to "qdo"
        QDO_DB_HOST -- defaults to localhost / unix domain sockets
        QDO_DB_PORT -- defaults to 5432
        QDO_DB_USER -- defaults to "qdo"
        QDO_DB_PASS -- defaults to None -- assuming use of ~/.pgpass

The table of queues has the concept of an "owner"; we want to support
multiple users using the same qdo database without stepping on each
other's toes.  This username ("owner") is controlled by the "qdo -u"
option.  Note, however, that nothing stops someone else from calling
"qdo -u" with *your* username; if the postgres database user account
can see it, qdo can see it.

-------------------------------
Database security and isolation
-------------------------------
There are three important concepts in the postgres back end: users, projects and
access rules. A project is a set of queues and tasks grouped under a single
identifier: e.g. project boss, containting 100 queues and their corresponding
tasks. Users are identified by a postgres user account. When a user is created
a project identified by the user's username is created (e.g. user john
has access to a project called john).

Access rules:
- A user can be enabled or disabled to access a database.
- Inside of a database, a user can be granted or revoked access to a project.
- By default a user has access to its default project,

This access rules are enforced by using postgres schemas. Each project is a
schema containing a instance of the tables described below. Users are granted
and revoked permission to use schemas' tables.

The project to be accessed can be set  in the qdo command by calling
"qdo -p/--project" with the intended project. 

Projects, users and user's projects are created by the AdminModule. Tables
created in the project can be seen at _create_project in the AdminModule class.

Dustin Lang, March 2015.
Gonzalo Rodrigo, August 2015
"""

from __future__ import print_function

import os
import time
import json
import random
import psycopg2

from ..core import Queue, Task, Backend, _worker

#-------------------------------------------------------------------------
#- Postgres specific Backend subclass

def _get_user(user):
    if user is None:
        user = os.environ.get('USER', 'qdo')
    return user

class _PostgresDBConnected(object):
    '''
    This is a context manager class for an open database connection.
    '''
    def __init__(self, db, do_commit=False):
        self.db = db
        self.do_commit = do_commit
    def __enter__(self):
        self.db._connect()
        if self.db._isolation_level != None:
            self.db.conn.set_isolation_level(self.db._isolation_level)
        return self.db
    def __exit__(self, exc_type, exc_value, traceback):
        if self.do_commit:
            self.db._commit()
        self.db._disconnect()
        # Return False: don't suppress any exceptions
        return False

class _VoidContextManager(object):
    '''
    This is a context manager class for an open database connection.
    '''
    def __init__(self):
        pass
    def __enter__(self):
        pass
    def __exit__(self, exc_type, exc_value, traceback):
        pass
        # Return False: don't suppress any exceptions
        return False

class _PostgresDB(object):
    '''
    Database wrapper class -- closes the connection & cursor after each query.
    '''
    def __init__(self, conn_dsn, retry_connect=True, n_retries=8, 
                 search_path=None):
        '''
        *conn_dsn*: a dictionary of "data source name" entries that
         will be passed to psycopg2.connect(**conn_dsn)
        '''
        self.conn_dsn = conn_dsn.copy()
        self.conn = None
        self.cursor = None
        self.retry_connect = retry_connect
        self.n_retries = n_retries
        self._search_path = search_path
        self._isolation_level = None
        
    def new(self):
        ''' Return a copy of this object. '''
        return _PostgresDB(self.conn_dsn, search_path=self._search_path)
        
    def connected(self):
        '''
        Returns a context manager if you want to run multiple commands
        with the database connection open.
        '''
        return _PostgresDBConnected(self)
    
    def connectedAndCommited(self, connected=False):
        '''
        Call equivalent to connected.
        
        Args:
            connected: bool, it indicates if it is called in a context
              where a connection already exists. 
        Returns: if connected is true, it returns a new 
             _PostgresDBConnected that will do a db._commit before
             disconnection. If false it Returns a context manager
             that does nothing but can be used with the *with* statement.
             
        '''
        if connected:
            return _VoidContextManager()
        else:
            return _PostgresDBConnected(self, do_commit=True)

    def execute(self, *args, **kwargs):
        ''' Execute an SQL command, returning nothing'''
        with self.connected():
            R = self._execute(*args, **kwargs)
            self._commit()
        return R
    def exec_one(self, *args, **kwargs):
        ''' Execute an SQL command and return a single result. '''
        with self.connected():
            R = self._exec_one(*args, **kwargs)
            self._commit()
        return R
    def executemany(self, *args, **kwargs):
        ''' Execute multiple SQL commands. '''
        with self.connected():
            R = self.cursor.executemany(*args, **kwargs)
            self._commit()
        return R
    def exec_all(self, *args, **kwargs):
        ''' Execute an SQL command and return all results. '''
        with self.connected():
            self._execute(*args, **kwargs)
            self._commit()
            R = self.fetchall()
        return R

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()
    
    def set_search_path(self, search_path):
        self._search_path = search_path

    def _connect(self):
        ''' Open real db connection '''
        if self.conn is None:
            backoff = 1
            retries = 0
            while True:
                try:
                    self.conn = psycopg2.connect(**self.conn_dsn)
                    break
                except:
                    if not self.retry_connect:
                        raise
                    if retries > self.n_retries:
                        print('Reached maximum number of postgres database connection retries')
                        raise
                    t = random.uniform(backoff, backoff*2)
                    print('Postgres database connection attempt failed; retrying after', t, 'seconds')
                    time.sleep(t)
                    retries += 1
                    backoff *= 2
                        
            self.conn.reset()
            self.cursor = self.conn.cursor()
            self._set_search_path()

    def _disconnect(self):
        ''' Close real db connection '''
        if self.cursor is not None:
            self.cursor.close()
            self.cursor = None
        if self.conn is not None:
            self.conn.rollback()
            self.conn.close()
            self.conn = None

    # Without connect/disconnect
    def _execute(self, *args, **kwargs):
        #print('_execute', args, kwargs)
        return self.cursor.execute(*args, **kwargs)
    def _exec_one(self, *args, **kwargs):
        self._execute(*args, **kwargs)
        return self.fetchone()
    def  _executemany(self, *args, **kwargs):
        ''' Execute multiple SQL commands. '''
        #print('_executemany', args, kwargs)
        return self.cursor.executemany(*args, **kwargs)
    def _commit(self):
        if self.conn is not None:
            return self.conn.commit()
    def _set_search_path(self):
        if self._search_path:
            self._execute('set search_path="'+self._search_path+'"')
            
    def set_isolation_level(self, level):
        self._isolation_level = level
        
    

    
class PostgresBackend(Backend):
    """postgre-based backend object"""
    def __init__(self, basedir=None):
        """
        Create a new PostgresBackend object

        Connects to:

        QDO_DB_NAME -- database name, defaults to "qdo"
        QDO_DB_HOST -- defaults to localhost / unix domain sockets
        QDO_DB_PORT -- defaults to 5432
        QDO_DB_USER -- defaults to "qdo"
        QDO_DB_PASS -- defaults to None -- assuming use of ~/.pgpass
        """
        dbname = os.environ.get('QDO_DB_NAME', 'qdo')
        host = os.environ.get('QDO_DB_HOST', None)
        port = os.environ.get('QDO_DB_PORT', None)
        if port is not None:
            port = int(port)
        user = os.environ.get('QDO_DB_USER', 'qdo')
        password = os.environ.get('QDO_DB_PASS', None)

        dsn = dict(database=dbname, user=user)
        if host is not None:
            dsn.update(host=host)
        if port is not None:
            dsn.update(port=port)
        if password is not None:
            dsn.update(password=password)

        self.db = _PostgresDB(dsn)

    def create(self, queuename, user=None, **kwargs):
        """
        Create a new queue and return a subclass of qdo.Queue

        *user*: name of the queue owner.
        
        Raises ValueError if queue_name already exists
        """
        return PostgresQueue(queuename, self.db.new(), user=user, mustcreate=True)

    def connect(self, queuename, user=None, create_ok=False, **kwargs):
        """
        Return a subclass of qdo.Queue representing queuename
        
        Optional inputs:
            user      : name of user who owns this queue (default $USER)
            create_ok : if True, create queue if needed.  If False (default),
                        raise ValueError if queue doesn't already exist.
        """
        return PostgresQueue(queuename, self.db.new(), user=user, cancreate=create_ok)

    def queues(self, user=None):
        """Return a list of qdo.Queue objects for known queues"""
        if user is None:
            res = self.db.exec_all('SELECT name,owner FROM queues ORDER BY id')
        else:
            res = self.db.exec_all('SELECT name,owner FROM queues ORDER BY id')
        from collections import OrderedDict
        queues = OrderedDict()
        for qname,owner in res:
            queues[qname] = PostgresQueue(qname, self.db.new(), user=owner)
        return queues
    
    def set_project(self, project):
        self.db.set_search_path(project)
        

qdoBackendClass = PostgresBackend

class PostgresQueue(Queue):
    def __init__(self, name, db, user=None, cancreate=False, mustcreate=False,
                 worker = None):
        super(PostgresQueue, self).__init__()
        self.name = name
        self.db = db
        user = _get_user(user)
        self._user = user
        
        if worker is None:
            self._worker = _worker()
        else:
            self._worker = worker
        
        if mustcreate:
            cancreate = True
        
        r = self.db.exec_one('SELECT id,name,owner FROM queues WHERE '
                             + 'name=%s', [name])
        if r is not None and mustcreate:
            raise ValueError('Queue {} already exists'.format(name))
        if r is None and not cancreate:
            raise ValueError('Queue {} does not exist'.format(name))
        if r is None:
            print('Creating queue', name, 'with owner', user)
            r = self.db.exec_one('INSERT INTO queues (name, owner, active) ' +
                                 'VALUES (%s,%s,%s) RETURNING id',
                                 (name, user, True))

        assert(r is not None)
        self.qid = r[0]

    @property
    def user(self):
        return self._user
    
    #--------------------------------------------------------------------
    def _add(self, task, priority=0.0, requires=None, id=None):
        # id is added to keep the signature of the original call.
        if priority is None:
            priority = 0.0

        task = json.dumps(task)
        
        if requires is None:
            state = Task.PENDING
        else:
            state = Task.WAITING
        
        with self.db.connected():
            q = ('INSERT INTO tasks (queue_id, task, state, priority) VALUES ' +
                 '(%s,%s,%s,%s) RETURNING id')
            (id,) = self.db._exec_one(q, (self.qid, task, state, priority))
            #- Add dependencies, but then also check if they are already done
            if requires is not None:
                self._add_dependency(id, requires, connected=True)
                self._update_waiting_task_state(id, connected=True)
            self.db._commit()
        return id
    
    def _add_dependency(self, taskid, requires, connected=False):
        """
        Add dependencies for a task
        
        taskid : string task id
        requires : list of ids upon which taskid depends
        """
        with self.db.connectedAndCommited(connected):
            query = """INSERT INTO dependencies (queue_id, task_id, requires)  
                            VALUES (%s, %s, %s)"""
            if not isinstance(requires, list):
                self.db._execute(query, (self.qid, taskid, requires))
            else:
                args = [(self.qid, taskid, x) for x in requires]
                self.db._executemany(query, args)
    
    #- Use bulk inserts to add multiple items more efficiently
    def _add_multiple(self, tasks, priorities=None, requires=None, **kwargs):
        
        #- Convert tasks to JSON
        tasks = [json.dumps(x) for x in tasks]
        if priorities is None:
            priorities = [0.0,] * len(tasks)
        
        #- If no dependencies, directly insert into Pending state;
        #- otherwise insert into Waiting state
        if requires is None:
            initial_states = [Task.PENDING,] * len(tasks)
        else:
            initial_states = [Task.PENDING if (x is None) else Task.WAITING 
                              for x in requires]
        
        q = ('INSERT INTO tasks (queue_id, task, state, priority) ' +
             'VALUES (%s,%s,%s,%s) RETURNING id')
        ids = []

        # DIY connect / many queries / disconnect sequence here.
        with self.db.connected():
            for t,p, initial_state in zip(tasks, priorities, initial_states):
                (r,) = self.db._exec_one(q, (self.qid, t, initial_state, p))
                ids.append(r)
            self.db._commit()
        #- Add dependencies if needed
        if requires is not None:
            for taskid, depids in zip(ids, requires):
                if depids is not None:
                    ### print(taskid, "depends upon", depids)
                    self._add_dependency(taskid, depids)
                    self._update_waiting_task_state(taskid)
                    
        return ids

    #- Get / Set state of the queue
    def _state(self):
        (active,) = self.db.exec_one('SELECT active FROM queues WHERE id=%s', (self.qid,))
        if active:
            return Queue.ACTIVE
        return Queue.PAUSED

    @property
    def state(self):
        return self._state()

    @state.setter
    def state(self, state):
        if state not in (Queue.ACTIVE, Queue.PAUSED):
            raise ValueError("Invalid queue state %s; should be %s or %s" % (state, Queue.ACTIVE, Queue.PAUSED))
        self.db.execute('UPDATE queues SET active=%s WHERE id=%s', (state == Queue.ACTIVE, self.qid))

    #- Get a task
    def _get(self, timeout=None, jobid=None):
        #- First make sure we aren't paused
        if self.state == Queue.PAUSED:
            return None
        
        t0 = time.time()
        while True:
            # SELECT ... FOR UPDATE locks the resulting row (if any) for
            # update, so that multiple qdo instances (perhaps running
            # on different nodes) don't retrieve the same task.
            getone = '''SELECT id, task, state, priority FROM tasks
                    WHERE state=%s AND queue_id=%s
                    ORDER BY priority DESC, id LIMIT 1 FOR UPDATE'''
            with self.db.connected():
                r = self.db._exec_one(getone, (Task.PENDING, self.qid))
                if r is not None:
                    # Got one!
                    (taskid,task,state,priority) = r
                    # set_task_state includes a commit() (via db.execute())
                    self._set_task_state(taskid, Task.RUNNING, jobid=jobid)
                    break

            # No tasks
            if time.time() - t0 > timeout:
                ### print("No tasks in queue; giving up")
                return None
            else:
                twait = min(timeout, 5)
                print("No tasks in %s; waiting %d seconds" % (self.name, twait))
                time.sleep(twait)

        return Task(json.loads(task), queue=self, id=taskid, state=Task.RUNNING, priority=priority)
         
    def _set_task_state(self, taskid, state, err=0, message=None, 
                        connected=False, jobid=None):

        with self.db.connectedAndCommited(connected):
            if state == Task.RUNNING and jobid is not None:
                update = "UPDATE tasks SET state=%s, err=%s, message=%s, jobid=%s WHERE id=%s"
                data = (state, err, message, jobid, taskid)
            else:
                update = "UPDATE tasks SET state=%s, err=%s, message=%s WHERE id=%s"
                data = (state, err, message, taskid)
            self.db._execute(update, data)
            if state in (Task.SUCCEEDED, Task.FAILED):
                self._update_waiting_tasks(taskid, connected=connected)

    def _retry(self, state=Task.FAILED, exitcode=None, priority=None, taskfilter=None):
        tasks = self.tasks(state=state, exitcode=exitcode)
        if taskfilter is not None:
            tasks = [t for t in tasks if taskfilter(t)]
        taskids = [t.id for t in tasks]
        if len(taskids) > 0:
            with self.db.connected():
                update_str = 'UPDATE tasks SET state=%s'
                update_args = (Task.WAITING,)

                if priority is None:
                    pass
                else:
                    update_str += ', priority=%s'
                    update_args += (priority,)

                if exitcode is None and taskfilter is None:
                    #- Faster if no id filtering is needed
                    where_str = 'WHERE queue_id=%s AND state=%s'
                    where_args = (self.qid, state)
                    self.db._execute(update_str + ' ' + where_str,
                                     update_args + where_args)
                else:
                    where_str = 'WHERE id=%s'
                    self.db._executemany(update_str + ' ' + where_str,
                                         [update_args + (tid,) for tid in taskids])

                #- Check if any Pending tasks should now move back to Waiting
                self.db._execute('SELECT id FROM tasks WHERE queue_id=%s AND state=%s',
                                 (self.qid, Task.PENDING,))
                results = self.db.fetchall()
                pending_taskids = [x[0] for x in results]
                for id in pending_taskids:
                    # print("Checking", id)
                    self._update_waiting_task_state(id, force=True, 
                                                    connected=True)
                self.db._commit()
        for id in taskids:
            with self.db.connected():
                self._update_waiting_task_state(id, connected=True)
                self.db._commit()
        return taskids

    #- functions to update states based on dependencies
    def _update_waiting_task_state(self, taskid, force=False, connected=False):
        """
        Check if all dependencies of this task have finished running.
        If so, set it into the pending state.
        
        if force, do the check no matter what.  Otherwise, only proceed
        with check if the task is still in the Waiting state.
        """
        with self.db.connectedAndCommited(connected):
            #- Ensure that it is still waiting
            #- (another process could have moved it into pending)
            if not force:
                q = 'SELECT state FROM tasks where tasks.id = %s'
                try:
                    state = self.db._exec_one(q, (taskid,))[0]
                except StopIteration:
                    raise ValueError("TaskID %s not found" % str(taskid))
                if state != Task.WAITING:
                    return
             
            #- Count number of dependencies that are still pending or waiting
            count_unfinished = """\
            SELECT COUNT(d.requires)
            FROM dependencies d JOIN tasks t ON d.requires = t.id
            WHERE d.task_id = %s AND t.state IN (%s, %s, %s)
            """
            try:
                n = self.db._exec_one(count_unfinished, 
                                     (taskid,Task.PENDING, Task.WAITING, 
                                      Task.RUNNING))[0]
    
            except StopIteration:
                return
            #TODO(gonzalorodrigo): look if it would be better to cancel
            # tasks that are children of a failed task.
            if n == 0:
                self._set_task_state(taskid, Task.PENDING, connected=connected)
            elif force:
                self._set_task_state(taskid, Task.WAITING, connected=connected)
          
    
    def _update_waiting_tasks(self, taskid, connected=False):
        """
        Identify any tasks that are waiting for taskid, and call
        _update_waiting_task_state() on them.
        """
        with self.db.connectedAndCommited(connected):
            q = """\
            SELECT id FROM tasks t JOIN dependencies d ON d.task_id = t.id
            WHERE d.requires = %s AND t.state = %s
            """
   
            self.db._execute(q, (taskid,Task.WAITING))
            waiting_tasks = self.db.fetchall()
                
            for taskid, in waiting_tasks:
                self._update_waiting_task_state(taskid, connected=connected)

        
    def _rerun(self, priority=None):
        failed = self._retry(Task.FAILED, priority=priority)
        succeeded = self._retry(Task.SUCCEEDED, priority=priority)
        return failed+succeeded
     
    def _pause(self):
        self.state = Queue.PAUSED
     
    def _resume(self):
        self.state = Queue.ACTIVE
     
    def _delete(self):
        self.db.execute('DELETE FROM dependencies where queue_id=%s', (self.qid,))
        self.db.execute('DELETE FROM tasks WHERE queue_id=%s', (self.qid,))
        self.db.execute('DELETE FROM queues WHERE id=%s', (self.qid,))
        self.db = None
        self.qid = None

    def _tasks(self, id=None, state=None, summary=False, exitcode=None):
        """
        Return list of Task objects for this queue.

        if state != None, return only tasks in that state
            (see Tasks.VALID_STATES)

        if id != None, return just that Task object.

        if summary=True, return dictionary of how many tasks are in each state.
        
        if exitcode is not None and id is None, return only tasks that
        exited with that exit code.
        """
        if (id is not None) and (exitcode is not None):
            raise ValueError("don't give both id and exitcode")

        #- If summary, just return count of tasks in each state
        if summary:
            ntasks = dict()
            count = 'SELECT count(state) FROM tasks WHERE queue_id=%s AND state=%s'
            with self.db.connected():
                for state in Task.VALID_STATES:
                    ntasks[state] = self.db._exec_one(count, (self.qid, state))[0]
            return ntasks

        results = list()
        query = 'SELECT id,task,state,err,message,jobid,priority FROM tasks WHERE queue_id=%s'
        args = [self.qid]
        if state is not None:
            query += ' AND tasks.state=%s'
            args.append(state)
        if id is not None:
            query += ' AND tasks.id=%s'
            args.append(id)
         
        query += ' ORDER BY id'
        
        #- If id is set, get just that task
        if id is not None:
            r = self.db.exec_one(query, args)
            if r is None:
                raise ValueError("Task ID {} doesn't exist".format(id))

            id,jsontask,taskstate,err,message,jobid,priority = r
            task = Task(json.loads(jsontask), self, id=id, state=taskstate,
                        err=err, message=message, jobid=jobid, priority=priority)
            return task
            
        #- Otherwise return them all
        else:
            tasks = self.db.exec_all(query, args)
            for id,jsontask,taskstate,err,message,jobid,priority in tasks:
                #- filter by exitcode here instead of in query to make sure
                #- it is current exit code, not some previous one for same
                #- task that was rerun.
                if (exitcode is None) or (exitcode == err):
                    task = Task(json.loads(jsontask), self, id=id, state=taskstate,
                                err=err, message=message, jobid=jobid, priority=priority)
                    results.append(task)
            return results

class AdminModule(object):
    """Class with basic functions to adminstrate a QDO postgress database
    and its users. Its main faunctions is to: create a new db, create/delete
    users, enables/disable users, create/detroy projects, grant/revoke users 
    access to projects."""
    
    def __init__(self, host, db_name, port, user, password):
        self._host = host
        self._db_name = db_name
        self._port = port
        self._user = user
        self._password = password
        self._refresh_db()
        
    def _refresh_db(self):
        self._dsn = dict(database=self._db_name, host=self._host,
                         port=self._port, user=self._user,
                         password=self._password)
        self._db = _PostgresDB(self._dsn)
    
    def create_new_db(self, db_name): 
        try:
            print("Creating database", db_name)
            self._db.set_isolation_level(0)
            create_db_query = (
                """CREATE DATABASE """+db_name+
                """ WITH OWNER = """+self._user+""";""")
            self._db.execute(create_db_query)
            print("Created.")
            self._db.set_isolation_level(1)
            return self.configure_db(db_name)
        except Exception as e:
            print("Database creating failed:", e)
            return False
        
    def configure_db(self, db_name): 
        try:
            print("Configuring database", db_name)
            self._db.set_isolation_level(0)
            print("Setting database permissions.")
            permissions_first = (
                """REVOKE CONNECT, TEMPORARY ON DATABASE """+db_name+
                """ FROM public;""")
            self._db.execute(permissions_first)
            self._db_name = db_name
            self._refresh_db()
         
            
            permissions =(
                """REVOKE ALL ON SCHEMA public FROM public;
                create role """+self._get_role_name()+""" nologin;
                grant connect on database """+db_name+
                """ to """+self._get_role_name())
            
            self._db.execute(permissions)
            self._db.set_isolation_level(1)
            print("Permissions set.")
            return True
        except Exception as e:
            print("Database configuration failed:", e)
            return False
    
    def destroy_db(self, db_name):
        try:
            self._db.set_isolation_level(0)
            print("Destroying database", db_name)
            drop_db_query = (
                """DROP DATABASE """+db_name+""";""")
            self._db.execute(drop_db_query)
            print("Dropped.")
            
            print("Destroying access role")

            permissions =(
                """DROP role """+self._get_role_name_db(db_name)+""";""")
            self._db.execute(permissions)
            print("Destroyed")
            
            self._db.set_isolation_level(1)
            return True
            
        except Exception as e:
            print("Database destruction failed:", e)
            return False
    
    def create_projects(self, projects):
        try:
            for prj in projects:
                print("Creating project", prj)
                self._create_project(prj, self._user)
                print("Created.")
            return True
        except Exception as e:
            print("Project creation failed:", e)
            return False
            
    def create_user(self, user_name, password = None):
        print("->Creating and configuring user", user_name)

        try:
            print("Creating user", user_name)
            self._create_user(user_name, password=password)
            if self.create_projects([user_name]):
                if self.grant_user_projects(user_name, [user_name]):
                    print("->Created and configured")
                return True
            else:
                print("Warning, user was created but a project with its name"
                      " already existed.")
        except Exception as e:
            print("User creation failed:", e)
        print("->User creation and configuration failed.")
        return False
                
    def list_users(self):
        print("->List of users")
        print("->+/- shows the user login access.")
        print("user_name(+/-): project1, project2..")
        print("-------------------------------------------------------")
        users, projects = self._list_users_projects()
        for user in users.keys():
            the_user=users[user]
            line = user +"("+self._get_enabled_symbol(the_user["enabled"]) + \
                "): ";
            for project in the_user["projects"]:
                line+=project+" "
            print(line)
    
    def list_projects(self):
        print("->List of projects and granted users.")
        print("->+/- shows the user login access.")
        print("project_name: user1(+/-), user2(+/-)...")
        print("---------------------------------------")
        users, projects = self._list_users_projects()
        for project in projects:
            line = project +": "
            for user in projects[project]:
                line+=(user+"("+self._get_enabled_symbol(users[user]["enabled"])+
                    ") ")
            print(line)
    
    def _get_enabled_symbol(self, val):
        if val:
            return "+"
        else:
            return "-"
    
    def grant_user_projects(self, user, projects):
        try:
            for project in projects:
                print("Granting user", str(user), "access to project",
                      str(project))
                self._grant_user_project(user, project)
                print("Granted.")
            return True
        except Exception as e:
            print("Grant operation failed:", e)
            return False
    
    def grant_project_users(self, project, users):
        try:
            for user in users:
                print("Granting user", str(user), "access to project",
                      str(project))
                self._grant_user_project(user, project)
                print("Granted.")
            return True
        except Exception as e:
            print("Grant operation failed:", e)
            return False
            
    def revoke_user_projects(self, user, projects):
        try:
            for project in projects:
                print("Revoking user", str(user), "access to project",
                      str(project))
                self._revoke_user_project(user, project)
                print("Rokoked.")
            return True
        except Exception as e:
            print("Revoke operation failed:", e)
            return False
    
    def revoke_project_users(self, project, users):
        try:
            for user in users:
                print("Revoking user", str(user), "access to project",
                      str(project))
                self._revoke_user_project(user, project)
                print("Revoked.")
            return True
        except Exception as e:
            print("Revoke operation failed:", e)
            return False
    
    def delete_projects(self, projects):
        try:
            for project in projects:
                print("Deleting project", project)
                self._delete_project(project)
                print("Deleted.")
            return True
        except Exception as e:
            print("Delete project failed:", e)
            return False
    
    def delete_users(self, users):
        try:
            for user in users:
                print("Deleting user", user)
                self._delete_user(user)
                print("Deleted.")
            return True
        except Exception as e:
            print("Delete user failed:", e)
            return False
    
            
    def disable_users(self, users):
        try:
            for user in users:
                print("Disabling user login", user)
                self._disable_user(user)
                print("Disabled.")
            return True
        except Exception as e:
            print("Disable user failed:", e)
            return False
           
    
    def enable_users(self, users):
        try:
            for user in users:
                print("Enabling user login", user)
                self._enable_user(user)
                print("Enabled.")
            return True
        except Exception as e:
            print("Enabling user failed:", e)
            return False
    
    def _create_project(self, project_name, owner):
        create_query = ("""
            CREATE SCHEMA """+self._quote(project_name)+
            """ AUTHORIZATION """+self._quote(owner)+ """;
            
            SET search_path="""+self._quote(project_name)+""";
            CREATE SEQUENCE queues_id_seq;
            
            CREATE TABLE queues (
                id bigint primary key not null default nextval('queues_id_seq'::regclass),
                name    text,
                owner   text,
                active  boolean
            );
            
            CREATE SEQUENCE tasks_id_seq;
            
            CREATE TABLE tasks (
                id bigint primary key not null default nextval('tasks_id_seq'::regclass),
                queue_id bigint REFERENCES queues(id),
                task     text,
                state    text,
                priority real,
                err     INTEGER default 0,
                message TEXT,
                jobid   TEXT
            );
            
            CREATE TABLE dependencies (
              queue_id bigint,
              task_id  bigint,
              requires bigint,
              FOREIGN KEY(queue_id) REFERENCES queues(id),
              FOREIGN KEY(task_id) REFERENCES tasks(id),
              FOREIGN KEY(requires) REFERENCES tasks(id)
            );     
            
            CREATE INDEX tasks_queue_id_idx ON tasks(queue_id);
            CREATE INDEX task_priority on tasks(priority);
            """)
        self._db.execute(create_query)
    
    def _create_user(self, user_name, password = None):
        if (not  password):
            password = ""
        create_query =("""    
            CREATE ROLE """+self._quote(user_name)+""" LOGIN 
            PASSWORD %s;
            GRANT """+self. _get_role_name() +
            """ TO """+self._quote(user_name)+""";
            """)
        self._db.execute(create_query, (password,))
    
    def _list_users_projects(self):
        active_users = self._get_active_users()
        query=("select s.nspname as project, u.rolname as user"
               " from pg_catalog.pg_namespace s,"
               " aclexplode(s.nspacl) as x join pg_roles u on x.grantee=u.oid"
               " where x.grantee!=x.grantor;")
        res = self._db.exec_all(query)
        users = dict()
        projects = dict()
        
        for project, user in res:
            if not project in projects.keys():
                projects[project]=[]
            if not user in users.keys():
                users[user] = dict(enabled=(user in active_users), projects=[])
            if user not in projects[project]:
                projects[project].append(user)
            if project not in users[user]:
                users[user]["projects"].append(project)
        return users, projects
    
    def _get_active_users(self):
        query=("select u.rolname username from pg_auth_members m,"
               " pg_roles u, pg_roles u2 where m.member=u.oid and"
               " m.roleid=u2.oid and u2.rolname=%s")
        res = self._db.exec_all(query, (self._get_role_name(),))
        return [x for (x,) in res];
              
    def _enable_user(self, user_name):
        create_query =("""    
            GRANT """+self. _get_role_name() +
            """ TO """+self._quote(user_name)+""";
            """)
        self._db.execute(create_query)
    
    def _disable_user(self, user_name):
        create_query =("""    
            REVOKE """+self. _get_role_name() +
            """ FROM """+self._quote(user_name)+""";
            """)
        self._db.execute(create_query)    
    
    def _grant_user_project(self, user_name, project_name):
        grant_query = ("""
        GRANT USAGE ON SCHEMA """+self._quote(project_name)+""" 
            TO """+self._quote(user_name)+""";
        GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES 
            IN SCHEMA """+self._quote(project_name)+""" 
            TO """+self._quote(user_name)+""";
        GRANT USAGE, SELECT, UPDATE ON ALL SEQUENCES 
            IN SCHEMA """+self._quote(project_name)+"""
            TO """+self._quote(user_name)+""";
            """)
        self._db.execute(grant_query)
    
    def _revoke_user_project(self, user_name, project_name):
        grant_query = ("""
        REVOKE USAGE ON SCHEMA """+self._quote(project_name)+""" 
            FROM """+self._quote(user_name)+""";
        REVOKE SELECT, INSERT, UPDATE, DELETE ON ALL TABLES 
            IN SCHEMA """+self._quote(project_name)+""" 
            FROM """+self._quote(user_name)+""";
        REVOKE USAGE, SELECT, UPDATE ON ALL SEQUENCES 
            IN SCHEMA """+self._quote(project_name)+"""
            FROM """+self._quote(user_name)+""";
            """)
        self._db.execute(grant_query)
    
    def _delete_project(self, project_name):
        delete_query = ("""
        DROP SCHEMA """+self._quote(project_name)+""" CASCADE """)
        self._db.execute(delete_query)
    def _delete_user(self, user_name):
        delete_query = ("""
        DROP OWNED BY """+self._quote(user_name) + """;
        DROP USER """+self._quote(user_name))
        self._db.execute(delete_query)
    def _quote(self, cad):
        return '"'+cad+'"'

    def _get_role_name(self):
        return self._get_role_name_db(self._db_name)
    
    def _get_role_name_db(self, db_name):
        return db_name +"_connect"
    
    

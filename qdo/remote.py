"""
Remote module offers similar functionalities to the backend but without
requiring to create Task of Queue objects. This allows the REST Server to do
oprations through sremote in a single call.

#- Load queue
import qdo
q = qdo.conect('EchoChamber')
q.add("echo hello")

#- Is equivalent to
import qdo.remote as remote

remote.add_task('EchoChamber', "echo hello')

Gonzalo Rodrigo, Summer 2015
"""

from __future__ import print_function

from . import core

def get_version():
    """Returns QDO version. Used to check that the library is accesible."""
    return core.__version__

def qlist(user=None):
    """Return a list of summaries for for known queues"""
    queues = core.qlist(user=user) 
    val= [q.status() for q in queues]
    return val 

def status(qname, user=None):
    """Returns status of a queue of a user."""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    return q.status()
    

def create(qname, user=None):
    """Returns status of a queue of a user."""
    try:
        q = core.create(qname, user=user)
    except ValueError as e:
        return None
    return q.status()
    

def retry_queue(qname, user=None):
    """Move qname's 'Failed' tasks into 'pending'"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    q.retry()
    return q.status()
    

def recover_queue(qname, user=None):
    """Move qname's 'Running' tasks into 'pending'"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    q.recover()
    return q.status()


def rerun_queue(qname, user=None):
    """Change completed tasks to pending in queue qname."""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    q.rerun()
    return q.status()


def delete_queue(qname, user=None):
    """Delete queue of a user."""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    q.delete()
    return True


def set_state(qname, state, user=None):
    """Sets state of a queue of a user."""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    if state == 'Paused':
        q.pause()
    elif state == 'Active':
        q.resume()
    return q.status()



def get_tasks(qname, state=None, user=None):
    """Returns the summary of the tasks of a queue with a certain state."""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        print(e)
        return None
    tasks = q.tasks(state=state)
    return [dict(t) for t in tasks]

    
def add_task(qname, task, user=None, **kwargs):
    """Adds a task to a queue"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    new_taskid = q.add(task, **kwargs)
    new_task = q.tasks(id=new_taskid)
    return dict(new_task)

def _clean_uniq(deps):
    if isinstance(deps, list):
        return [str(x) if (x!=None) else None for x in deps]
    if deps is not None:
        return str(deps)
    else:
        return None
         

def add_multiple_tasks(qname, tasks, user=None,
                       ids=None, priorities=None, requires=None):
    """Adds multiple tasks to a queue"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    requires=_clean_uniq(requires)
    ids=_clean_uniq(ids)
    list_ids = q.add_multiple(tasks, ids=ids, priorities=priorities,
                              requires=requires)
    list_created_tasks=[]
    for new_taskid in list_ids:
        new_task = q.tasks(id=new_taskid)
        list_created_tasks.append(dict(new_task))
    
    return list_created_tasks

    
    
def launch_queue(qname, nworkers, user=None, njobs = 1,
                   mpiproc_per_worker=0, cores_per_mpiproc=None, 
                   cores_per_worker=1,
                   numa_pack=False,
                   script=None, timeout=None, runtime=None, batchqueue=None,
                   walltime=None,
                   batch_opts='', verbose=False,
                   export_all_env_vars=False, extra_vars_to_export = []):
    """Launches a worker to process tasks of the queue. Same interface as
    backend"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    q.launch(nworkers, njobs = njobs,
                   mpiproc_per_worker=mpiproc_per_worker, 
                   cores_per_mpiproc=cores_per_mpiproc, 
                   cores_per_worker=cores_per_worker,
                   numa_pack=numa_pack,
                   script=script, timeout=timeout, runtime=runtime, 
                   batchqueue=batchqueue,
                   walltime=walltime,
                   batch_opts=batch_opts, verbose=verbose,
                   export_all_env_vars=export_all_env_vars,
                   extra_vars_to_export=extra_vars_to_export)
    return True

def burndown_queue(qname, epoch_start, epoch_end, bin_size, user=None):
    """Returns the burndown data of a queue"""
    try:
        q = core.connect(qname, user=user)
    except ValueError as e:
        return None
    
    return q.burndown(epoch_start, epoch_end, bin_size)
             
    

! MPI program that prints the ID of the CPUs executing each MPI PE.
! Requires mycpu.c to be compiled.
! To compile:
!   icc -c mycpu.c 
!   ftn -o helloworld -openmp helloworld.f90 my_cpu.o
!
program helloWorld
 implicit none
 include "mpif.h"
 integer :: myPE, numProcs, ierr, len, rc, cpu, FINDMYCPU
 character*(MPI_MAX_PROCESSOR_NAME) name
 external FINDMYCPU
 call MPI_INIT(ierr)
 call MPI_COMM_RANK(MPI_COMM_WORLD, myPE, ierr)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, numProcs, ierr)
 call MPI_GET_PROCESSOR_NAME(name, len, ierr)
 if (ierr .ne. MPI_SUCCESS) then
     print *,'Error getting processor name. Terminating.'
     call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
 end if
 cpu = FINDMYCPU()
 print *, "Hello from Processor ", myPE, " ", numProcs, " ", trim(name),":", cpu
 call MPI_FINALIZE(ierr)
end program helloWorld

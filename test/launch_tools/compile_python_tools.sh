#!/bin/bash
#
# Creates the python cpumodule that exposes compumodele.getmycpu() which returns
# and integer with the id of the CPU that is running the call.
# 
# Usage of module:
#   import cpumodule
#   print("Mu cpu id is "+str(cpumodule.getpmycpu))
#

module load swig
swig -python cpumodule.i
icc -O2 -fPIC -c cpumodule.c
icc -O2 -fPIC -c cpumodule_wrap.c -I/usr/include/python
icc -shared cpumodule.o cpumodule_wrap.o -o _cpumodule.so:wq

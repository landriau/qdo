/*
 * C module to encapsulate the system call sched_getcpu().
 * This call only exists in Linux.
 *
 */
#include "cpumodule.h"
#include <utmpx.h>

int sched_getcpu();

int getmycpu()
{
    int cpu;
    cpu = sched_getcpu();
    return cpu;
}

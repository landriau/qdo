#!/bin/bash
#
# Script to compile MPI and MPI+OMP test tools.
# Requires ICC, FTN with support for OPM and MPI.
# The final binares are appended with the name of the NERSC system running them.
#
if [ -n "${NERSC_HOST+1}" ] ; then
	echo "Compiling for ${NERSC_HOST}"
else
	NERSC_HOST="generic"
	echo "Compiling for no host in particular"
fi

lib_name="my_cpu_${NERSC_HOST}.o"
icc -c mycpu.c -o $lib_name
ftn -o omp_${NERSC_HOST} -openmp omp.f90 $lib_name
ftn -o helloworld_${NERSC_HOST} -openmp helloworld.f90 $lib_name

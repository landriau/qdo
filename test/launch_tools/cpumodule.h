/*
 * C module to encapsulate the system call sched_getcpu().
 * This call only exists in Linux.
 *
 * Created to be imported in Python with SWIG.
 */

/*
 * Returns an int with the id of the CPU executing this code.
 */
int getmycpu();

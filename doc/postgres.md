Postgres Backend
================

This document presents who the postgres backend was designed. This is
specially important to understand how the database was structured. 

These are the basic concepts in the system:

* User: Identified by a username string. It identifies a qdo user within the
backend. It translates into a postgres role.

* Project: Set of grouped queues identified by a string. It can be
accessed by n users. All users have at least a default project with the same
name as the user name. A project translates into a database schema (private
view of the overall database).

# Database model #


The base database is created with the following instructions:

    CREATE DATABASE qdo
    WITH OWNER = db_owner;
    
    REVOKE CONNECT, TEMPORARY ON DATABASE qdo FROM public;
    
    \connect qdo
    
    REVOKE ALL ON SCHEMA public FROM public;
    
    create role qdo_connect nologin;
    grant connect on database qdo to qdo_connect;
    
## Projects and schema ## 
The models uses the idea of schema: subdatabases of the same database. A 
database user can operate on a schema creating/destroying tables and 
inserting/deleting/querying data without affecting any of the other schema.

This allows to map *projects* on *schema*: A project is composed by a number
of queues, that are contained in a table within a schema of the same name.
A user switching projects translates accessing different schema. This
operation is down within postgres by using: 

    set search_path=project_name

while invoking QDO the user can select the project by using:

* API:

    qdo.set_project('project_name')
    
* CMD:
    
    qdo ... --project 'project_name' ...
 
## Project access ##
Access to projects is controlled through postgres roles. A user will be able
to access a project if its postgres roles is granted access to the corresponding
schema.

This is controlled by the the qdo_pgadmin command and corresponding API.

# Setting up a the postgres backend #

## Creating the database from scratch ##
The user must have administrator powers over the postgres server. It can be done
by calling

    qdo_pgadmin createdb 'dbnametobecreated'
    
It will create the database and basic permission model. Tables are created
when users and projects are created.

## Setting up an existing database ##
In case that the server is not administrated by the qdo admin, there is
a command just to configure an exiting database. It requires the user to
have full permissions on the database.

    qdo_pgadmin configdb 'exitingdbname'

It will just configure the permission model. The database should be empty.

## Administration ##

The postgres administration is done through the qdo_pgadmin command. It allows
to: 
* Create/setup databases
* Create/delete users
* Create/delete projects
* Grant/revoke user access to projects (one user to *n* projects, *n* users to
one project). 
* List projects and their corresponding users.
* List users and their granted projects.

Read qdo_postmen command line help.



